var _WM_APP_PROPERTIES = {
  "activeTheme" : "simply-red",
  "dateFormat" : "",
  "defaultLanguage" : "en",
  "displayName" : "chatter",
  "homePage" : "Main",
  "name" : "chatter",
  "platformType" : "MOBILE",
  "supportedLanguages" : "en",
  "timeFormat" : "",
  "type" : "APPLICATION",
  "version" : "1.0"
};