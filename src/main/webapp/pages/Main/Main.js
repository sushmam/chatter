Application.$controller("MainPageController", ["$scope", "$timeout", function($scope, $timeout) {
    "use strict";

    /* perform any action on widgets/variables within this block */
    $scope.onPageReady = function() {
        /*
         * variables can be accessed through '$scope.Variables' property here
         * e.g. to get dataSet in a staticVariable named 'loggedInUser' use following script
         * $scope.Variables.loggedInUser.getData()
         *
         * widgets can be accessed through '$scope.Widgets' property here
         * e.g. to get value of text widget named 'username' use following script
         * '$scope.Widgets.username.datavalue'
         */
    };


    $scope.chatroomonMessageReceive = function(variable, data) {
        $timeout(function() {
            // var l = WM.element('.chatcontent').focus();
            WM.element('.chatcontent').animate({
                scrollTop: WM.element('.chatcontent')[0].scrollHeight
            }, 1000);
        })
    };

}]);